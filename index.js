import { wsUrl, mdWsUrl, makeMessage, makeTicks } from './api.js'

async function logTick(tick) {
  console.log(JSON.stringify(tick, null, 2))
  return
}

const api = await makeMessage(wsUrl)
const md  = await makeMessage(mdWsUrl)
await api.waitForOpen()
await api.sendRequest('authorize')
await md.waitForOpen()
await md.sendRequest('authorize')

//grab the latest nasdaq 100 futures contract
const symbol = (await api.sendRequest({ request: 'contract/suggest', query: [['t', 'NQ'], ['l', 1]] }))[0]['name']
const { history, nextTick } = await makeTicks({ md: md, symbol: symbol, elementsBack: 5 })
console.log(JSON.stringify(history, null, 2))
console.log(history.length)

//there can be duplicate tick['time'] values with different tick['volume'] values presumably for subsecond granularity
let tick = await nextTick()
while (true) {
  await logTick(tick)
  tick = await nextTick()
}

//shouldn't get here
await api.closeWebSocket()
await md.closeWebSocket()

