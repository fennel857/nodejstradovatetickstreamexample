import { readFile, writeFile } from 'fs/promises'
import { request } from 'https'
//npm install ws
import WebSocket from 'ws'

//const urlBase = 'live.tradovateapi.com'
const urlBase = 'demo.tradovateapi.com'
//market data feed, has the getChart endpoint and doesn't have the https authorization endpoint and doesn't have the contract suggestion endpoint
const mdUrlBase = 'md.tradovateapi.com'
//const mdUrlBase = 'md-demo.tradovateapi.com'

const urlVersion = 'v1'

const httpsUrl        = 'https://' + urlBase   + '/' + urlVersion
export const wsUrl    = 'wss://'   + urlBase   + '/' + urlVersion + '/websocket'
export const mdWsUrl  = 'wss://'   + mdUrlBase + '/' + urlVersion + '/websocket'

/*
load the key and keyExpiration from keyPath if the file exists and the key is still valid
if not then get a key using the login information from loginPath and write the key and keyExpiration to keyPath for persistence across program executions
*/
async function makeGetKey(options) {
  const { loginPath, keyPath } = options
  let key
  let keyExpiration
  
  async function authorize(loginJsonPath) {
    try {
      const login = JSON.parse(await readFile(loginJsonPath))
      let response = await tryToConnect(login)
      while (response['p-ticket'] !== undefined) {
        if (response['p-captcha'] !== undefined) {
          throw 'authorization p-captcha, wait for an hour and try again'
        }
        delay(response['p-time'] * 1000)
        response = await tryToConnect({...login, 'p-ticket': response['p-ticket']})
      }
      const { errorText, accessToken, expirationTime } = response
      if (errorText !== undefined) {
        throw errorText
      }
      key = accessToken
      keyExpiration = expirationTime
      await writeFile(keyPath, JSON.stringify({'key': key, 'keyExpiration': keyExpiration}))
      return
    }
    catch (err) {
      console.error(err)
      process.exitCode = 1
    }
  }

  //get a key using the https endpoint
  async function tryToConnect(login) {
    return new Promise((resolve, reject) => {
      try {
        const loginString = JSON.stringify(login)
        const options = {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Content-Length': loginString.length
          }
        }
        const req = request(httpsUrl + '/auth/accesstokenrequest', options, (res) => {
          if (res.statusCode !== 200) {
            throw ('authorization post request status code ' + res.statusCode)
          }
          let chunks = ''
          res.setEncoding('utf8')
          res.on('data', (chunk) => { chunks += chunk })
          res.on('end', () => { resolve(JSON.parse(chunks)) })
          res.on('error', (err) => { throw err })
        })
        req.end(loginString)
      }
      catch (err) {
        reject(err)
      }
    })
  }
  
  async function keyValid() {
    return ((new Date(keyExpiration)) - (new Date())) > 0
  }
  
  async function getKey() {
    if (key === undefined) {
      //load the key from keyPath if it exists
      try {
        let json = JSON.parse(await readFile(keyPath))
        key = json['key']
        keyExpiration = json['keyExpiration']
        //console.log('reusing old key')
      }
      catch {}
    }
    //if it still hasn't been loaded or if it's expired then authorize another one
    if (key === undefined || !(await keyValid())) {
      await authorize(loginPath)
    }
    return key
  }

  return getKey
}

//every request gets a unique id
async function makeNextId() {
  let count = 0
  return async () => {
    let oldCount = count
    count += 1
    return oldCount
  }
}

//first in first out
function makeSynchronousQueue() {
  let count = 0
  let start = 0
  let m = {}
  function push(e) {
    m[count] = e
    count += 1
    return e
  }
  function pop() {
    let e = undefined
    if (start < count) {
      e = m[start]
      delete m[start]
      start += 1
    }
    return e
  }
  function peek() {
    let e = undefined
    if (start < count) {
      e = m[start]
    }
    return e
  }
  return { push: push, pop: pop, peek: peek }
}

//asynchronous first in first out
async function makeQueue() {
  let resolveQueue = makeSynchronousQueue()
  let dataQueue = makeSynchronousQueue()
  function push(e) {
    dataQueue.push(e)
    if (resolveQueue.peek() !== undefined) {
      const resolve = resolveQueue.pop()
      resolve(dataQueue.pop())
    }
    return e
  }
  async function pop() {
    const resolveEmpty = resolveQueue.peek() === undefined
    const dataEmpty = dataQueue.peek() === undefined
    if (!resolveEmpty && !dataEmpty) {
      const resolve = resolveQueue.pop()
      resolve(dataQueue.pop())
      return new Promise((resolve, reject) => { resolveQueue.push(resolve) })
    }
    else if ((!resolveEmpty && dataEmpty) || (resolveEmpty && dataEmpty)) {
      return new Promise((resolve, reject) => { resolveQueue.push(resolve) })
    }
    else if (resolveEmpty && !dataEmpty) {
      return new Promise((resolve, reject) => { resolve(dataQueue.pop()) })
    }
  }
  return { push: push, pop: pop }
}

//starts a websocket connection and lets the client wait for request responses and wait for events
export async function makeMessage(wsUrl) {
  const ws = new WebSocket(wsUrl)
  const nextId = await makeNextId()
  const getKey = await makeGetKey({ loginPath: 'login.json', keyPath: 'key.json' })
  let responses = {}
  let eventQueue = await makeQueue()
  const nextEvent = eventQueue.pop
  let resolveClose
  const closePromise = new Promise((resolve, reject) => { resolveClose = resolve })
  let resolveOpen
  const openPromise = new Promise((resolve, reject) => { resolveOpen = resolve })
  
  ws.on('message', async (message) => {
    try {
      let interval
      const type = message.slice(0, 1)
      message = message.slice(1, message.length)
      //open frame
      if (type === 'o') {
        resolveOpen()
      }
      //heartbeat frame
      else if (type === 'h') {
        ws.send('[]')
      }
      //array frame
      else if (type === 'a') {
        const messages = JSON.parse(message)
        messages.forEach((eventOrResponse) => {
          //event
          if (eventOrResponse['e'] !== undefined) {
            //const { e, d } = eventOrResponse
            eventQueue.push(eventOrResponse)
          }
          //response
          else if (eventOrResponse['i'] !== undefined) {
            const { i, s, d } = eventOrResponse
            if (!((s >= 200) && (s <= 299))) {
              throw ('response status ' + s + ' for ' + JSON.stringify(eventOrResponse))
            }
            const resolveResponse = responses[i]
            if (resolveResponse === undefined) {
              throw ('response id ' + i + ' not found')
            }
            delete responses[i]
            resolveResponse(d)
          }
          else { throw ('unknown array frame message ' + JSON.stringify(eventOrResponse)) }
        })
      }
      //close frame
      else if (type === 'c') {
      }
      else { throw ('unknown message type ' + type) }
    }
    catch (err) {
      ws.close()
      console.error(err)
      process.exitCode = 1
    }
  })
  
  ws.on('close', () => { resolveClose() })

  async function waitForOpen() {
    return openPromise
  }

  async function closeWebSocket() {
    ws.close()
    return closePromise
  }

  //if options is just a string then do the authorize endpoint with a valid key
  //otherwise get the request name, queries, and body from options
  async function sendRequest(options) {
    try {
      let request
      let queries
      let body
      if (typeof(options) === 'string') {
        request = options
        queries = []
        body = request === 'authorize' ? (await getKey()) : ''
      }
      else {
        request = options['request']
        queries = options['query']
        body = options['body']
        if (request === undefined) {
          throw 'no request'
        }
        if (queries === undefined) {
          queries = []
        }
        if (body === undefined) {
          body = ''
        }
        else {
          body = JSON.stringify(body)
        }
      }
      const id = await nextId()
      const promise = new Promise((resolve, reject) => { responses[id] = resolve })
      let queryString = ''
      if (queries.length >= 1) {
        const lastQuery = queries.pop()
        queries.forEach((query) => { queryString += (query[0] + '=' + query[1] + '&') })
        queryString += (lastQuery[0] + '=' + lastQuery[1])
      }
      //console.log(request + '\n' + id + '\n' + queryString + '\n' + body)
      ws.send(request + '\n' + id + '\n' + queryString + '\n' + body)
      return promise
    }
    catch (err) {
      console.error(err)
      process.exitCode = 1
    }
  }

  return { waitForOpen: waitForOpen, closeWebSocket: closeWebSocket, sendRequest: sendRequest, nextEvent: nextEvent }
}

//download historical data going back elementsBack ticks and make a nextTick function to get current data as it comes
export async function makeTicks(options) {
  const { md, symbol, elementsBack } = options
  const timeRange = { asMuchAsElements: elementsBack }
  let nextTickId
  const queue = makeSynchronousQueue()
  //history is the ticks before the first end of history flag
  //ticks after the first eoh flag are current and can be waited for with nextTick
  let history = []
  let ticks = {}

  async function nextPackets() {
    let e = await md.nextEvent()
    while (e['e'] !== 'chart') {
      e = await md.nextEvent()
    }
    return e['d']['charts']
  }

  function eohInPackets(packets) {
    for (let i = 0; i < packets.length; i += 1) {
      if (packets[i]['eoh'] !== undefined) {
        return i
      }
    }
    return -1
  }

  function makeTick(packet, oldTick) {
    let tick = {}
    tick['time']   = packet['bt'] + oldTick['t']
    tick['price']  = packet['bp'] + oldTick['p']
    tick['volume'] = oldTick['s']
    tick['id']     = oldTick['id']
    return tick
  }

  async function getChart(timeRange) {
    //const { mode, historicalId, realtimeId } = await md.sendRequest({ request: 'md/getChart', body: {
    const { realtimeId } = await md.sendRequest({ request: 'md/getChart', body: {
      symbol: symbol,
      chartDescription: {
        underlyingType: 'Tick',
        elementSize: 1,
        elementSizeUnit: 'UnderlyingUnits',
        withHistogram: false
      },
      timeRange: timeRange}})
    return realtimeId
  }

  function packetsToTicks(packets) {
    return packets.map((packet) => { return packet['tks'].map((oldTick) => { return makeTick(packet, oldTick) }) }).flat()
  }

  //wait for the ticks before the first eoh flag and sort them
  //the next tick id to wait for is the last tick id of the history + 1
  async function getHistory() {
    //let realtimeId = await getChart({ closestTimestamp: startDate, asFarAsTimestamp: endDate })
    //let realtimeId = await getChart({ closestTimestamp: startDate })
    //let realtimeId = await getChart({ closestTimestamp: endDate })
    //use this to get current data as it comes
    //let realtimeId = await getChart({ asMuchAsElements: 1 })
    let realtimeId = await getChart(timeRange)
    let packets = await nextPackets()
    let eohIndex
    while ((eohIndex = eohInPackets(packets)) === -1) {
      for (let tick of packetsToTicks(packets)) { history.push(tick) }
      packets = await nextPackets()
    }
    for (let tick of packetsToTicks(packets.slice(0, eohIndex))) { history.push(tick) }
    //sort in ascending order
    history.sort((a, b) => { return a['id'] - b['id'] })
    for (let packet of packets.slice((eohIndex + 1), packets.length)) { queue.push(packet) }
    //make the history length elementsBack by removing 0 or more of the oldest ticks
    history = history.slice(history.length - elementsBack, history.length)
    //get the tick id after the newest tick
    nextTickId = history[history.length - 1]['id'] + 1
    return history
  }

  //ignore ticks that are behind the current tick and cache ticks that are after the current tick
  function storePacket(packet) {
    let nextTick
    for (let tick of packetsToTicks([packet])) {
      if (tick['id'] === nextTickId) {
        nextTick = tick
      }
      else if (tick['id'] > nextTickId) {
        ticks[tick['id']] = tick
      }
    }
    return nextTick
  }

  //get the next tick, wherever it is
  async function nextTick() {
    //store the packets that exist already
    for (let packet = queue.pop(); packet !== undefined; packet = queue.pop()) {
      let nextTick = storePacket(packet)
      if (nextTick !== undefined) {
        return nextTick
      }
    }
    //the tick has already been stored
    if (ticks[nextTickId] !== undefined) {
      let tick = ticks[nextTickId]
      nextTickId += 1
      return tick
    }
    //wait for the tick to show up
    let nextTick
    while (nextTick === undefined) {
      let packets = await nextPackets()
      packets = packets.filter((packet) => { return packet['tks'] !== undefined })
      for (let packet of packets) {
        let maybeNextTick = storePacket(packet)
        if (maybeNextTick !== undefined && nextTick === undefined) {
          nextTick = maybeNextTick
          nextTickId = nextTick['id'] + 1
        }
      }
    }
    return nextTick
  }

  return { history: (await getHistory()), nextTick: nextTick }
}

